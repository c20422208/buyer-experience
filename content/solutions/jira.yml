---
  title: GitLab with Jira
  description: Automate your work from GitLab to Jira
  components:
    - name: 'solutions-hero'
      data:
        title: GitLab with Jira
        subtitle: Automate your work from GitLab to Jira
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /learn/#agile_management
          text: Start Learning
          data_ga_name: start learning
          data_ga_location: header
        # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab with jira"
    - name: featured-media
      data:
        column_size: 4
        media:
          - title: GitLab <-> Jira Integration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              Once you integrate your GitLab project with your Jira instance, you can automatically detect and cross-reference activity between the GitLab project and any of your projects in Jira.
            icon:
              name: kanban
              alt: Kanban
              variant: marketing
          - title: Jira Dev Panel Integration
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Complementary to our existing Jira project integration, you’re now able to integrate GitLab projects with Jira Development Panel.
            icon:
              name: computer-test
              alt: Jira Dev Panel Integration
              variant: marketing
          - title: Migrate from Jira to GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              GitLab offers a robust project management tool that brings your agile planning into one platform that hosts your SCM, CI/CD, Security and more!
            icon:
              name: scale
              alt: Jira Dev Panel Integration
              variant: marketing
    - name: featured-media
      data:
        header: How GitLab-Jira Integration Works
        column_size: 4
        media:
          - title: Basic GitLab <-> Jira Integration
            aos_animation: zoom-in-up
            aos_duration: 500
            text: |
              * Mention a Jira issue ID in a commit message or MR (merge request)
              &nbsp;
              * Mention that a commit or MR resolves or closes a specific Jira issue
              &nbsp;
              * View Jira issues directly in GitLab
            image:
              url: /nuxt-images/enterprise/source-code.jpg
              alt: "GitLab <-> Jira"
          - title: Jira Dev Panel Integration
            aos_animation: zoom-in-up
            aos_duration: 1000
            text: |
              * Easily access related GitLab merge requests, branches, and commits directly from a Jira issue
              &nbsp;
              * Works with self-managed GitLab or GitLab.com integrated with Jira hosted by your Jira Cloud
              &nbsp;
              * Connects all GitLab projects within a top-level group or a personal namespace to projects in the Jira instance
            image:
              url: /nuxt-images/enterprise/ci-cd.jpg
              alt: "Panel integration"
          - title: Migrate from Jira to GitLab
            aos_animation: zoom-in-up
            aos_duration: 1500
            text: |
              * Import your Jira issues to GitLab.com or to your self-managed GitLab instance
              &nbsp;
              * Import title, description and labels directly
              &nbsp;
              * Map Jira users to GitLab project members
            image:
              url: /nuxt-images/enterprise/agile2.jpg
              alt: "Migrate from GitLab to Jira"
    - name: featured-media
      data:
        header: How-To Videos
        text: |
          Here’s a list of resources on the GitLab-Jira integration that we've created to be particularly helpful in understanding implementation.
        column_size: 4
        media:
          - title: GitLab-Jira Basic Integration
            aos_animation: fade-up
            aos_duration: 500
            text: |
              While you can always migrate content and process from Jira to GitLab Issues, you can also opt to continue using Jira and use it together with GitLab
            link:
              text: Read
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira integration
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/fWvwkx5_00E?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLab-Jira Development Panel
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              Complementary to our existing Jira project integration, you’re able to integrate GitLab projects with Jira Development Panel
            link:
              text: Read
              href: https://docs.gitlab.com/ee/integration/jira/
              data_ga_name: Gitlab-Jira development panel
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/VjVTOmMl85M?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Import your Jira project issues to GitLab
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              Using GitLab Jira importer, you can import your Jira issues to GitLab.com or to your self-managed GitLab instance.
            link:
              text: Read
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Import jira issue to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: View Jira issue list in GitLab
            aos_animation: fade-up
            aos_duration: 500
            text: |
              For organizations using Jira as their primary work tracking tool, it can be a challenge for contributors to work across multiple systems and maintain a single source of truth.
            link:
              text: Read
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: View jira issue list in GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/_yDcD_jzSjs?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: Map Jira users to GitLab users when importing issues
            aos_animation: fade-up
            aos_duration: 1000
            text: |
              When importing issues from Jira to GitLab, you can now map Jira users to GitLab project members prior to running the import. This enables the importer to set the correct reporter and assignee on the issues you’re moving to GitLab.
            link:
              text: Read
              href: https://docs.gitlab.com/ee/user/project/import/jira/
              data_ga_name: Map jira users to GitLab
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/cixs4NJB3H8?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - title: GitLab Project Management Roadmap
            aos_animation: fade-up
            aos_duration: 1500
            text: |
              We're constantly working on improving GitLab's integration with Jira so we welcome feedback or check what's coming in the next releases
            link:
              text: Read
              href: https://gitlab.com/groups/gitlab-org/-/epics/2738/
              data_ga_name: GitLab project management roadmap
              data_ga_location: features
            video:
              url: https://www.youtube.com/embed/bT60rJEoWhw?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: logo-links
      data:
        header: Open Source Partners
        column_size: 2
        logos:
          - logo_url: /nuxt-images/enterprise/logo-dish.svg
            logo_alt: Dish
            aos_animation: zoom-in
            aos_duration: 200
          - logo_url: /nuxt-images/enterprise/logo-expedia.svg
            logo_alt: Expedia
            aos_animation: zoom-in
            aos_duration: 400
          - logo_url: /nuxt-images/enterprise/logo-goldman-sachs.svg
            logo_alt: Goldman Sachs
            aos_animation: zoom-in
            aos_duration: 600
          - logo_url: /nuxt-images/enterprise/logo-nasdaq.svg
            logo_alt: Nasdaq
            aos_animation: zoom-in
            aos_duration: 800
          - logo_url: /nuxt-images/enterprise/logo-uber.svg
            logo_alt: Uber
            aos_animation: zoom-in
            aos_duration: 1000
          - logo_url: /nuxt-images/enterprise/logo-verizon.svg
            logo_alt: Verizon
            aos_animation: zoom-in
            aos_duration: 1200
